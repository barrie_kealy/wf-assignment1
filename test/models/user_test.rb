require 'test_helper'

class UserTest < ActiveSupport::TestCase

  def setup
    @user = User.new(first_name: "Barrie", surname: "Kealy", age: 25, 
    email: "user@example.com", home_address: "Carlow", password: "secretPassword", password_confirmation: "secretPassword")
  end
  
  test "first name should be present" do
    @user.first_name = "     "
    assert_not @user.valid?
  end
  
  test "surname should be present" do
    @user.surname = "     "
    assert_not @user.valid?
  end
  
  test "age name should be present" do
    @user.age = "     "
    assert_not @user.valid?
  end

  test "email should be present" do
    @user.email = "     "
    assert_not @user.valid?
  end
  
  test "email addresses should be unique" do
    duplicate_user = @user.dup
    duplicate_user.email = @user.email.upcase
    @user.save
    assert_not duplicate_user.valid?
  end
  
  test "address name should be present" do
    @user.home_address = "     "
    assert_not @user.valid?
  end
  
   test "password should be present (nonblank)" do
    @user.password = @user.password_confirmation = " " * 6
    assert_not @user.valid?
  end

  test "password should have a minimum length" do
    @user.password = @user.password_confirmation = "a" * 5
    assert_not @user.valid?
  end
end