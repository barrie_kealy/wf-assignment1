class User < ApplicationRecord
    validates :first_name, presence: true, length: { maximum: 50 }
    validates :surname, presence: true, length: { maximum: 50 }
    validates :age, presence: true
    VALID_EMAIL_REGEX = /\A[\w+\-.]+@[a-z\d\-.]+\.[a-z]+\z/i
    validates :email, presence: true, length: { maximum: 255 }, format: { with: VALID_EMAIL_REGEX }, uniqueness: { case_sensitive: false }
    validates :home_address, presence: true, length: { maximum: 255 }
    before_save { self.email = email.downcase }
    has_secure_password
    validates :password, presence: true, length: { minimum: 6 }
end
