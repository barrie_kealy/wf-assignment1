class CreateUsers < ActiveRecord::Migration[5.1]
  def change
    create_table :users do |t|
      t.string :first_name
      t.string :surname
      t.integer :age
      t.string :email
      t.string :home_address

      t.timestamps
    end
  end
end
